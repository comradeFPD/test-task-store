<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @extends Factory
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    #[ArrayShape([
        'title' => 'string',
        'slug'  => 'string',
        'qty'   => 'integer',
        'price' => 'float'
    ])]
    public function definition(): array
    {
        $title = fake()->unique()->jobTitle;
        return [
            'title' => $title,
            'slug'  => Str::slug($title),
            'qty'   => fake()->randomNumber(),
            'price' => fake()->randomFloat('2', 0, 7000)
        ];
    }
}
