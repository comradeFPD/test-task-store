<h1>How to install project</h1>
<ol>
<li> Для запуска проекта локально можно использовать либо Docker либо развернуть проект на локальном сервере</li>
<li> Необходимо выполнить команду <b>php artisan key:generate</b></li>
<li> Затем следует команда <b>php artisan migrate --seed</b></li>
<li>Данные для пользователя email: <b>admin@mail.com</b> password: <b>password</b></li>
</ol>
