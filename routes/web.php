<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('products', [ProductController::class, 'index'])
    ->name('products.index');
Route::get('products/{id}', [ProductController::class, 'show'])
    ->name('products.show');
Route::post('add-to-cart', [CartController::class, 'addToCart'])
    ->name('cart.add-item');
Route::put('delete-from-cart', [CartController::class, 'deleteFromCart'])
    ->name('cart.delete-item-from-cart');
Route::get('cart', [CartController::class, 'showCart'])
    ->name('cart.show');
Route::post('make-order', [OrderController::class, 'create'])
    ->name('order.create');
Route::middleware('auth:web')->group(function () {
    Route::get('order/{id}', [OrderController::class, 'show'])
        ->name('order.show');
    Route::get('orders', [OrderController::class, 'index'])
        ->name('order.index');
    Route::delete('order/{id}', [OrderController::class, 'delete'])
        ->name('order.delete');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
