<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;

/**
 * @package App\Models
 *
 * @class Order
 *
 * @property-read  int $id
 * @property float $total
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read Product[]|Collection|null $products
 */
class Order extends Model
{
    use HasFactory;

    public function products() : BelongsToMany
    {
        return $this->belongsToMany(
            Product::class,
            'orders_products_pivot',
            'order_id',
            'product_id'
        )->withPivot('quantity');
    }
}
