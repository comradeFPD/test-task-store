<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderProductPivot extends Pivot
{
    use HasFactory;

    public function order() : BelongsToMany
    {
        return $this->belongsToMany(Order::class, 'order_id', 'id');
    }

    public function product() : BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_id', 'id');
    }
}
