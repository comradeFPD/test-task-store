<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @package App\Models
 *
 * @class Product
 *
 * @property-read int $id
 * @property string $title
 * @property string $slug
 * @property int $qty
 * @property float $price
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read Order[]|Collection|null $orders
 */
class Product extends Model
{
    use HasFactory;

    public function orders() : BelongsToMany
    {
        return $this->belongsToMany(
            Order::class,
            'orders_products_pivot',
            'product_id',
            'order_id'
        );
    }
}
