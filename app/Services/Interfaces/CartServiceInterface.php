<?php

namespace App\Services\Interfaces;

interface CartServiceInterface
{
    /**
     * Add product to cart
     *
     * @param int $productId
     * @param int $qty
     *
     * @return void
     */
    public function addToCart(int $productId, int $qty) : void;

    /**
     * Delete product from cart
     *
     * @param int $productId
     * @param int $qty
     *
     * @return void
     */
    public function deleteFromCart(int $productId, int $qty) : void;

    /**
     * Get cart items
     *
     * @return array
     */
    public function showCart() : array;

    /**
     * Update product quantity in storage
     *
     * @param int $productId
     * @param int $qty
     * @param string $type
     *
     * @return void
     */
    public function updateProductQuantityInStorage(int $productId, int $qty, string $type) : void;
}
