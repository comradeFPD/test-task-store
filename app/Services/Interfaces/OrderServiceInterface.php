<?php

namespace App\Services\Interfaces;

use App\Models\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface OrderServiceInterface
{

    /**
     * Create new order
     *
     * @return void
     */
    public function create() : void;

    /**
     * Delete single order
     *
     * @param int $id
     *
     * @return void
     */
    public function delete(int $id) : void;

    /**
     * Get all orders
     *
     * @return LengthAwarePaginator
     */
    public function index() : LengthAwarePaginator;
}
