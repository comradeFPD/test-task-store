<?php

namespace App\Services\Interfaces;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface ProductServiceInterface
{
    /**
     * Show all products
     *
     * @return LengthAwarePaginator
     */
    public function index() : LengthAwarePaginator;

    /**
     * Show single product
     *
     * @param int $id
     *
     * @return Product|Model
     */
    public function show(int $id) : Product|Model;
}
