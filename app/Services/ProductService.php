<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductService implements Interfaces\ProductServiceInterface
{

    /**
     * @inheritDoc
     */
    public function index(): LengthAwarePaginator
    {
        return Product::query()->paginate(20);
    }

    /**
     * @inheritDoc
     */
    public function show(int $id): Product|Model
    {
        return Product::query()->findOrFail($id);
    }
}
