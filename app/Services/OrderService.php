<?php

namespace App\Services;

use App\Models\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class OrderService implements Interfaces\OrderServiceInterface
{

    /**
     * @inheritDoc
     */
    public function create(): void
    {
        $service = new CartService();
        $order = new Order();
        $order->total = 0;
        $cart = $service->showCart();
        $products = [];
        foreach ($cart as $key => $value){
            $order->total += $value['qty'] * $value['price'];
            $products[$key] = ['quantity' => $value['qty']];
        }
        $order->save();
        $order->products()->sync($products);
        session()->forget('cart');
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        $order = Order::query()->findOrFail($id);
        $order->delete();
    }

    /**
     * @inheritDoc
     */
    public function index(): LengthAwarePaginator
    {
        return Order::query()->paginate(15);
    }
}
