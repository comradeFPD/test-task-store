<?php

namespace App\Services;

use App\Models\Product;

class CartService implements Interfaces\CartServiceInterface
{

    /**
     * @inheritDoc
     */
    public function addToCart(int $productId, int $qty): void
    {
        $cart = $this->showCart();
        /**
         * @var Product $product
         */
        $product = Product::query()->findOrFail($productId);
        if(isset($cart[$productId])){
            $cart[$productId]['qty']++;
        } else {
            $cart[$productId] = [
                'qty'   => $qty,
                'title' => $product->title,
                'img'   => $product->image,
                'price' => $product->price
            ];
        }
        $this->updateProductQuantityInStorage($productId, $qty, 'add');
        session()->put('cart', $cart);
    }

    /**
     * @inheritDoc
     */
    public function deleteFromCart(int $productId, int $qty): void
    {
        $cart = $this->showCart();
        if(isset($cart[$productId])){
            $cart[$productId]['qty'] -= $qty;
        }
        if($cart[$productId]['qty'] <= 0){
            unset($cart[$productId]);
        }
        session()->put('cart', $cart);
        $this->updateProductQuantityInStorage($productId, $qty, 'subtract');

    }

    /**
     * @inheritDoc
     */
    public function showCart(): array
    {
        return session()->get('cart', []);
    }

    /**
     * @inheritDoc
     */
    public function updateProductQuantityInStorage(int $productId, int $qty, string $type): void
    {
        $product = Product::query()->findOrFail($productId);
        if($type == 'add'){
            $product->qty -= $qty;
        } else {
            $product->qty += $qty;
        }
        $product->save();
    }


}
