<?php

namespace App\Http\Controllers;

use App\Http\Requests\Cart\AddToCart;
use App\Services\Interfaces\CartServiceInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class CartController extends Controller
{
    private CartServiceInterface $service;

    public function __construct(CartServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Add item to cart
     *
     * @param AddToCart $request
     *
     * @return RedirectResponse
     */
    public function addToCart(AddToCart $request) : RedirectResponse
    {
        $data = $request->validated();
        $this->service->addToCart($data['product_id'], $data['qty']);
        return redirect()->back()->with('message', 'Товар успешно добавлен');
    }

    /**
     * Delete item from cart
     *
     * @param AddToCart $request
     *
     * @return RedirectResponse
     */
    public function deleteFromCart(AddToCart $request) : RedirectResponse
    {
        $data = $request->validated();
        $this->service->deleteFromCart($data['product_id'], $data['qty']);
        return redirect()->back()->with('message', 'Товар успешно удален');
    }

    /**
     * Show cart
     *
     * @return Response
     */
    public function showCart() : Response
    {
        $cart = $this->service->showCart();
        $total = 0;
        foreach ($cart as $item){
            $total += $item['qty'] * $item['price'];
        }
        return response()->view('cart.show', [
            'cart' => $cart,
            'total' => $total
        ]);
    }
}
