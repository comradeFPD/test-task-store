<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\ProductServiceInterface;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    private ProductServiceInterface $service;

    /**
     * Class constructor
     *
     * @param ProductServiceInterface $service
     */
    public function __construct(ProductServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Get all products paginated by 20
     *
     * @return Response
     */
    public function index() : Response
    {
        return response()->view('products.index', [
            'products' => $this->service->index()
        ]);
    }

    /**
     * Show single product
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(int $id) : Response
    {
        return response()->view('products.show', [
            'product' => $this->service->show($id)
        ]);
    }
}
