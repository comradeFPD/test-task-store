<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Services\Interfaces\OrderServiceInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    private OrderServiceInterface $service;

    /**
     * Class constructor
     *
     * @param OrderServiceInterface $service
     */
    public function __construct(OrderServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Create order
     *
     * @return RedirectResponse
     */
    public function create() : RedirectResponse
    {
        $this->service->create();
        return redirect()->route('products.index')->with('message', 'Заказ успешно создан');
    }


    /**
     * Show all orders
     *
     * @return Response
     */
    public function index() : Response
    {
        return response()->view('orders.index', [
            'orders' => $this->service->index(),
            'total' => Order::query()->sum('total')
        ]);
    }

    public function delete(int $id)
    {

    }
}
