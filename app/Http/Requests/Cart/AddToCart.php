<?php

namespace App\Http\Requests\Cart;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

class AddToCart extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    #[ArrayShape([
        'product_id' => 'integer',
        'qty'        => 'integer'
    ])]
    public function rules(): array
    {
        return [
            'product_id' => 'required|exists:products,id',
            'qty'        => 'required|min:1|numeric'
        ];
    }
}
