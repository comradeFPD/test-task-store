@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            @if(!empty($cart))
            @foreach($cart as $key => $item)
                <div class="col-md-4">
                    <div class="card">
                        <a href="{{ route('products.show', $key) }}" class="link-info">
                            <p class="card-header text-center">{{ $item['title'] }}</p>
                            <img class="card-img"  src="{{ $item['img'] ?? 'cart.png' }}" alt="Product image" width="300" height="300">
                        </a>
                        <p class="card-text">В корзине: {{ $item['qty'] }}</p>
                        @if($item['qty'] > 0)
                            @include('layouts.cart._delete-from-cart-button')
                        @endif
                    </div>
                </div>
            @endforeach
            <div class="row">
                <h2>Сумма товаров в заказе: {{ $total }}</h2>
            </div>
            @include('layouts.cart._order-button')
            @else
            <h2 class="text-center">Ваша корзина пуста!</h2>
                @endif
        </div>
    </div>
@endsection
