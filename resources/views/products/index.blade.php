@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
        @foreach($products as $product)
                <div class="col-md-4">
                    <div class="card">
                        <a href="{{ route('products.show', $product->id) }}" class="link-info">
                            <p class="card-header text-center">{{ $product->title }}</p>
                        <img class="card-img"  src="{{ $product->image ?? 'cart.png' }}" alt="Product image" width="300" height="300">
                        </a>
                        <p class="card-text">Количество на складе: {{ $product->qty }}</p>
                        <p class="card-text">Цена: {{ $product->price }}</p>
                        @if($product->qty > 0)
                        @include('layouts.cart._add-to-cart-button')
                            @endif
                    </div>
                </div>
        @endforeach
        </div>
    </div>
    @endsection
