@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="card">
                <h2 class="card-title text-center">{{ $product->title }}</h2>
                <img src="{{ $product->image ?? '/cart.png' }}" alt="Картинка товара" class="card-img">
                <p class="card-text">Продукта на складе: {{ $product->qty }}</p>
                @if($product->qty > 0)
                @include('layouts.cart._add-to-cart-button')
                @endif
            </div>
        </div>
    </div>
@endsection
