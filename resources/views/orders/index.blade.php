@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <table class="table table-hover">
                <tr>
                    <th>Номер заказа</th>
                    <th>Дата заказа</th>
                    <th>Товары</th>
                    <th>Стоимость</th>
                </tr>
                @foreach($orders as $order)
                    <tr>
                        <td> {{ $order->id }}</td>
                        <td> {{ $order->created_at }}</td>
                        <td> {{ implode(',', $order->products->pluck('title')->toArray()) }}</td>
                        <td> {{ $order->total }}</td>
                    </tr>
                @endforeach
            </table>
            <div class="row">
                <h3>Сумма всех заказов: {{ $total }}</h3>
            </div>
        </div>
    </div>
@endsection
