<div class="card-footer">
    <form action="{{ route('cart.add-item') }}" method="POST">
        @csrf
            <input type="hidden" value="{{ $product->id }}" name="product_id">
        <div class="form-group">
            <label class="form-label">Количество</label>
            <input id="qty" type="number" name="qty" class="form-text form-control" min="1" max="{{ $product->qty }}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success form-control">Добавить в корзину</button>
        </div>
    </form>
</div>
