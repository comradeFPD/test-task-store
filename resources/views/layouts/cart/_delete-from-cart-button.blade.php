<div class="card-footer">
    <form action="{{ route('cart.delete-item-from-cart') }}" method="POST">
        @csrf
        <input type="hidden" value="{{ $key }}" name="product_id">
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label class="form-label">Количество</label>
            <input id="qty" type="number" name="qty" class="form-text form-control" min="1" max="{{ $item['qty']}}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-danger form-control">Удалить из корзины</button>
        </div>
    </form>
</div>
